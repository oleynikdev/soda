﻿(function () {
    'use strict';

    angular
        .module('sodaApp')
        .factory('tabService', tabService);

    tabService.$inject = ['$http'];

    function tabService($http) {
        var service = {
            documents: {},
            ids: []
        };

        service.insetDoc = function (document) {

            service.documents[document.id] = {
            
                id: document.id,
                title: document.title,
                text: document.text,
                selected: document.selected
            };

            service.ids.push(document.id);
        }

        service.deleteDoc = function (id) {

            delete service.documents[id];

            service.ids.remove(id);

            console.log("service.deleteDoc");
            console.log(service.documents);
            console.log(service.ids);
        }

        service.select = function (id) {

            service.ids.move(service.ids.indexOf(id), 0);

            console.log("service.select");
            console.log(service.documents);
            console.log(service.ids);
        }

        return service;
    }
})();