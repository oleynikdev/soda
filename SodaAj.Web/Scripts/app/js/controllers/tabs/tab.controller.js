﻿(function () {
    'use strict';

    angular
        .module('sodaApp')
        .controller('tabCtr', tabCtr)
        .filter("orderByIds", ["tabService", function (tabService) {
            
            return function(input) {

                //console.log('filter: orderById');
                //console.log(tabService.ids);

                var array = [];
                var i = 1;

                angular.forEach(tabService.ids, function(id, key) {
                    console.log(i);
                    
                    array.push(tabService.documents[id]);

                    i++;
                });

                //console.log(array);

                return array;
            }
        }]);

    tabCtr.$inject = ['$scope', '$rootScope', 'tabService'];

    function tabCtr($scope, $rootScope, tabService) {
        $scope.service = tabService;

        var _id = 1;
        var _object = {
            "1": {
                id: "1",
                title: "Title 1"
            },
            "2": {
                id: "2",
                title: "Title 2"
            }
        };

       
        $scope.add = function() {

            tabService.insetDoc({
                id: _id,
                title: "Title " + _id,
                text: "The following example places the vertical pill menu inside the last column. So, on a large screen the menu will be displayed to the right. But on a small screen, the content will automatically adjust itself into a single-column layout",
                selected: false
            });

            _id++;
        }

        $scope.close = function(id) {

            tabService.deleteDoc(id);
        };

        $scope.select = function(id, index) {

            if (index >= $rootScope.hideFrom) {

                tabService.select(id);
            }
        };

        $scope.toArray = function() {
            
            //console.log('to array');
            //console.log(this);

            var array = [];
            var i = 1;

            //angular.forEach(_object, function (value, key) {
            //    console.log(i);
            //    array.push(value);
            //    i++;
            //});

            angular.forEach(tabService.ids, function (id, key) {
                console.log(i);
                array.push(tabService.documents[id]);
                i++;
            });

            return array;
        }
        //console.log($scope);

        $scope.show = function (id,point) {
            
            if (!(id && point))
                return;

            console.log(id);
            console.log(point);
        };

        $scope.foo = function (item, e) {
            console.log(item);
            console.log(e);
            console.log('foo');
        };

        activate();

        function activate() { }
    }
})();
