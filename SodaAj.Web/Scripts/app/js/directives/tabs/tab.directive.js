﻿(function() {
    'use strict';

    angular
        .module('sodaApp')
        .directive('tabDropDown', tabDropDown)
        .directive('onClickRigth', onClickRigth).directive('bernd', function () {
            return {
                restrict: 'E',
                scope: {
                    callback: '&'
                },
                link: function(scope) {
                    
                    var before = 44444;
                    var after = 8888;

                    scope.callback({
                        newValue: after,
                        oldValue: before
                    });
                }
            };
        });

    tabDropDown.$inject = ['$window', '$rootScope'];
    
    function tabDropDown($window, $rootScope) {
        // Usage:
        //     <tab></tab>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            scope: {
                
            }
        };
        return directive;

        function link(scope, element, attrs) {

            console.log("dir: tabDropDown");
            console.log(scope);

           $rootScope.hideFrom = 5;
        }
    }

    function onClickRigth() {
        // Usage:
        //     <tab></tab>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                callback: '&'
            }
        };

        return directive;

        function link(scope, element, attrs) {

            element.on('contextmenu', '.collapse', function (e) {
                e.preventDefault();
                
                console.log('contextmenu: ' + $(this).data('id'));

                scope.callback({
                    id: $(this).data('id'),
                    point: {
                        x: e.offsetX,
                        y: e.offsetY
                    }
                });
            });
        }
    }
})();